#!/bin/groovy

@Library("opssuite-jenkins-library@v2.0.8") _

pipeline {
    agent any
    parameters {
        string(name: 'testenv', defaultValue: 'qa3', description: 'Enter Environment')
        string(name: 'tags', defaultValue: 'zeroTestsToRun', description: 'Enter Tags')
        string(name: 'noOfThreads', defaultValue: '3', description: 'No of Browsers')

    }
    stages {

        stage ('Initialize and Checkout') {
            steps {
                cleanWs()
                checkout scm
            }
        }

        stage ('AWS Auth') {
            steps {
                script {
                    appBuildPipelineFunctions.loginToAws()
                }
            }
        }

        stage('Automated Integration Tests') {
            steps {
               sh './gradlew -DnoOfContainers=${noOfThreads} startEnvironment'
               sh './gradlew clean test -DincludedGroups=${tags} -Dtest.env=${testenv} -DnoOfThreads=${noOfThreads} -Dbrowser=chrome -Dremote=true'

            }
        }
    }

    post {
        always {
            sh './gradlew stopEnvironment'
            publishHTML (target: [
                    allowMissing: false,
                    alwaysLinkToLastBuild: false,
                    keepAll: true,
                    reportDir: 'build/extent-reports',
                    reportFiles: 'Report.html',
                    reportName: "Extent Report"
            ])

            emailext (
                subject: currentBuild.currentResult + ": Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]'",
                body: """<p>Build Status : ${currentBuild.currentResult} </p> <p>Job: ${env.JOB_NAME}</p> <p>Build Number: ${env.BUILD_NUMBER}:</p>
                <p>Check console output at &QUOT;<a href='${env.BUILD_URL}'>${env.JOB_NAME} [${env.BUILD_NUMBER}]</a>&QUOT;</p>""",
                to: 'pradeep.vatsavayi@wnco.com, Vamsiyadav.Jangala@wnco.com, Syam.Yalamanchili@wnco.com'
            )

        }

    }
}
